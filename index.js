/**
 * TEST
 */
const drivelist = require('drivelist');
const fs 		= require('fs-extra');
const fse  		= require('fs-extra');
const errorFile = "error_file.txt";

/** initial length, jumlah drive yang didapat saat pertama kali pengecekan*/
var initialLength;
var isFirst = false;
var isUnplug = false;
/** setInterval untuk pengecekah usb baru, dipakai di fungsi initialCheck */
var LoopAndCheck = null;
/** setInterval untuk pengecekan saat usb baru dilepas, dipakai pada fungsi checkAndCopy*/
var loopAndCheckUnplug = null;
var sourceName = "config_bca.js";
var serverSettingLoc = 'D:\yusuftest\\'+sourceName;
var state = "idle";


console.log("APPS STARTED");
initialCheck();

/** Mengecek jumlah drive yang dimiliki pc pertama kali, untuk perbandingan saat ditemukan usb baru*/
function initialCheck(){
	state = "initial_check";
	drivelist.list((error, drives) => {
		if (error) {
			console.log(error);
			setTimeout(initialCheck,2000);
			//throw error;
		}else if(!isFirst){
			//console.log(drives);
			initialLength = drives.length;
			isFirst = true;
			setTimeout(initialCheck,2000);
		}else if(initialLength != drives.length){
			checkAndCopy(drives);
		}else{
			setTimeout(initialCheck,2000);
		};
	})
}


/** fungsi dipanggil saat new usb ditemukan.
 * @constructor
 * @param {object} drive yang ditemukan saat proses initial check
 */
function checkAndCopy(drives){
	var isFound = false;
	//loop every drives
	for(var i=0;i<drives.length;i++)
	{
		//loop every drive's letter
		for(var j=0;j<drives[i].mountpoints.length;j++)
		{
			console.log("check at "+drives[i].mountpoints[j].path);
			//cek filename everyname
			try {
			    fs.accessSync(drives[i].mountpoints[j].path+"\\"+sourceName, fs.F_OK);
				console.log("FILE FOUND AT "+drives[i].mountpoints[j].path+"\\"+sourceName);
				isFound = true;
				//delete file at pc
				try{
					fs.unlinkSync(serverSettingLoc);
					console.log("DELETE SUCCESS");
				}catch(e){
					console.log("ERROR DELETING FILE : "+e);
				}//skip if no file

				try{
					console.log("START COPY FILE FROM :"+drives[i].mountpoints[j].path+"\\"+sourceName);
					fs.createReadStream(drives[i].mountpoints[j].path+"\\"+sourceName).pipe(fs.createWriteStream(serverSettingLoc));
					console.log("COPY SUCCESS");
				}catch(e){
					console.log("ERROR COPY FILE :"+e)
				}
				
				//start checking interval until usb unplug
			} catch (e) {
				//file ad mountpoint not found
			}
		}
	}

	if(!isFound)
	{
		console.log("-----WARNING-----"); 
		console.log("File di flashdisk baru tidak sesuai,");
		console.log("pastikan nama file adalah "+sourceName+", dan ditaruh di file paling atas");
	}

	console.log("Restart checking again");
	checkDrivesUnplug();
}

function checkDrivesUnplug()
{
	drivelist.list((error, drives) => {
		if (error) {
			console.log(error);
			console.log("SOMETHING ERROR");
			setTimeout(checkDrivesUnplug,1000);
		}else if(initialLength == drives.length)
		{
			isFirst = false;
			//DEVICE UNPLUGGED
			console.log("New Device Unplugged");
			console.log("Start checking device again");
			initialCheck();
		}else{
			setTimeout(checkDrivesUnplug,1000);
		}
	});
}